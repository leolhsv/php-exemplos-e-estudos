<?php

//Dependency injection

interface DatabaseDriver
{
    public function configure(array $config);
    public function connect();
}

class MogoDbDriver implements DatabaseDriver
{
    public function configure() {
        $this->config = $config;
    }

    public function connect () {
        $pdo = new \MogoClient($this->config['server'])
    }
}

class PdoDriver implements DatabaseDriver
{
    public function configure() {
        $this->config = $config;
    }

    public function connect () {
        $pdo = new \PDO($this->config['dsn'], $this->config['user'], $this->config['pass']); 
    }
}

class Database
{
    public function __construct(\DatabaseDriver $driver) {

        $this->driver = $pdo;
    }
}


$ioc = [];

$ioc['db_mogo'] = function () {
    $mongo_driver = new MongoDbDriver();
    $mongo_driver->configure([]);

    return new Database($mongo_driver);
};

$ioc['db'] = function () {
        $pdo_driver = new PdoDriver();
        $pdo_driver->configure([]);

        return new Database($pdo_driver);
};

$ioc['db'](); 