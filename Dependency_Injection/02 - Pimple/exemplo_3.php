<?php

require __DIR__.'/vendor/autoload.php';

use Pimple\Container;


class TestAdapter
{
    public function __construct() {
        var_dump(TestAdapter::class.'::__construct()');
    }
    public function runTest($message) {
        var_dump($message);
    }    
}

class Tester
{
    public function __construct(TestAdapter $adapter){
        $adapter->runTest('Rodou um Teste');
    }
}


$ioc = new Container;

$ioc['ta'] = function($c){
    return new TestAdapter;
};

$ioc['tester'] = function($c){
    return new Tester($c['ta']);
};

$tester = $ioc['tester'];