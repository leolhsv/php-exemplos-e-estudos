<?php

require __DIR__.'/../vendor/autoload.php';

use Zend\ServiceManager\ServiceManager;
use Zend\ServiceManager\Factory\InvokableFactory;

$serviceManager = new ServiceManager([
    'factories' => [
        'stdClass' => InvokableFactory::class 
    ]
]);

$objeto = $serviceManager->get('stdClass');

var_dump($objeto);