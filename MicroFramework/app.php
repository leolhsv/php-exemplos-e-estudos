<?php

require __DIR__.'/vendor/autoload.php';

$app = new LeoFig\App;
$app->setRenderer(new LeoFig\Renderer\PHPRenderer);

$app->get('/hello/{name}', function($params) {
    return $params;
});

$app->run();



