<?php

namespace LeoFig\DI;

class Resolver
{
    private $dependencies;

    public function method($method, array $dependencies = [])
    {
        $this->dependencies = $dependencies;
        
        $info = new \ReflectionFunction($method);
        $parameters = $info->getParameters();
        $parameters = $this->resolveParameters($parameters);        

        return call_user_func_array($info->getClosure(), $parameters);
    }

    public function class($class, array $dependencies = [])
    {
        $this->dependencies = $dependencies;

        $class = new \ReflectionClass($class);

        if (!$class->isInstantiable()) {
            throw new \Excpetion("{$class} is not instaciavel");
        }

        $constuctor = $class->getConstructor();

        if (!$constuctor) {
            return new $class->name;
        }

        $parameters = $constuctor->getParameters();
        $parameters = $this->resolveParameters($parameters);           

        return $class->newInstanceArgs($parameters);
    } 
    
    private function resolveParameters($parameters)
    {
        $dependencies = [];

        foreach ($parameters as $parameter) {
            $dependency = $parameter->getClass();

            if ($dependency) {
                $dependencies[] = $this->class($dependency->name, $this->dependencies);
            } else {
                $dependencies[] = $this->getDependencies($parameter);
            }
        }

        return $dependencies;
    }

    private function getDependencies($parameter) 
    {
        if (isset($this->dependencies[$parameter->name])) {
            return $this->dependencies[$parameter->name];
        }

        if ($parameter->isDefaultValueAvailable()) {
            return $parameter->getDefaultValue();
        }

        throw new \Excpeption("{parater->name} not receive a valid value");
    }
}