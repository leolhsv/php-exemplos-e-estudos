<?php

namespace LeoFig\Renderer;

class PHPRenderer implements PHPRendererInterface
{
    public function setData($data)
    {
        $this->data = $data;
    }

    public function run()
    {
        if (is_string($this->data)) {
            header('content-type:text/html; charset=UFT=8');
            echo $this->data;
            exit;
        }

        if (is_array($this->data)) {
            header('content-type:text/html; application/json');
            echo json_encode($this->data);
            exit;
        }

        throw new \Excpetion("Route return is invalid");
    }
}