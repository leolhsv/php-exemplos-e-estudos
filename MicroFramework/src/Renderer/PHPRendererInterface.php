<?php

namespace LeoFig\Renderer;

interface PHPRendererInterface
{
    public function setData($data);
    public function run();
}