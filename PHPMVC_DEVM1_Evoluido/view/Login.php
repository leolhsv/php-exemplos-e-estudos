<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">    
</head>
<body>
    <div class="container">
        <div class="jumbotron">
            <h1>Página de Login</h1>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="painel painel-default">
                    <div class="painel-body">
                        <div class="control-group">
                            <label for="">Login</label>
                            <input type="text" class="form-control" placeholder="login">
                        </div>
                        <div class="control-group">
                            <label for="">Senha</label>
                            <input type="password" class="form-control" placeholder="senha">
                        </div>
                        <div class="control-group">
                            <input type="submit" class="btn btn-success" value="Acessar">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>