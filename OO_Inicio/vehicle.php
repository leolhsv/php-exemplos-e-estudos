<?php 

require_once("IVehicle.php");

abstract class Vehicle implements IVehicle
{
    public $brand;
    protected $color;
    public $engine;

    public function __construct($brand, $color)
    {
        $this->brand = $brand;
        $this->color = $color;
    }

    public function getEngine($type = "teste")
    {
        return "{$this->engine} {$type}";
    } 

    abstract public function getMarca();   
}


 ?>