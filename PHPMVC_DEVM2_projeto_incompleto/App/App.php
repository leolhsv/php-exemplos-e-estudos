<?php

namespace App;

use App\Controllers\HomeController;
use Exception;

class App
{
    private $controller;
    private $controllerFile;
    private $action;
    private $params;
    public  $controllerName;

    public function __construct(){
        /*
         * Constantes do Sistema
         */
        define('APP_HOST'    , $_SERVER['HTTP_HOST']."/PHPMVC_DEVM2");
        define('PATH'        , realpath('./'));
        define('TITLE'       , "Primeira aplicação MVC em PHP - Devmedia");
        define('DB_HOST'     , "localhost");
        define('DB_USER'     , "root");
        define('DB_PASSWORD' , "Calvino#321");
        define('DB_NAME'     , "teste");
        define('DB_DRIVER'   , "mysql");

        $this->url();
    }


    public function run(){

        //Identifica qual é o controller
        if ($this->controller) {
            $this->controllerName = ucwords($this->controller) . 'Controller'; // ucwords transforma o primeiro caracter maiusculo concatenando com a palavra controller.
            $this->controllerName = preg_replace('/[^a-zA-Z]/i', '', $this->controllerName); // preg_replace, expressão regular que remove caracteres especiais diferentes de A-Z
        } else {
            $this->controllerName = "HomeController";
        }

        //Identifica a ação do controller (qual é o método)
        $this->controllerFile = $this->controllerName . '.php'; //O atributo controllerFile recebe o nome da classe controller e concatena com a extensão PHP para que seja verificada a existência deste arquivo mais a frente.
        $this->action         = preg_replace('/[^a-zA-Z]/i', '', $this->action); //Atributo action recebe o seu nome utilizando a expressão regular para remover qualquer caractere diferente de (A até Z e a até z).

        //Identifica quais são os parametros passados na ação (método)
        if (!$this->controller) {
            $this->controller = new HomeController($this);
            $this->controller->index();
        }

        //Verifica se arquivo da classe controller solicitada existe.
        if (!file_exists(PATH . '/App/Controller/' . $this->controllerFile )) { 
            throw new Excpetion("Página não encontrada.", 404);
        }

        $nomeClasse       = "\\App\\Controllers" . $this->controllerName;
        $objetoController = new $nomeClasse($this);

        if (!class_exists($nomeClasse)) {
            throw new Exception("Erro na aplicação.",500);
        }

        if (method_exists($objetoController, $this->action)) {
            $objetoController->{$this->action}($this->params);
            return;
        } else if (!$this->action && method_exists($objetoController, 'index')) {
           $objetoController->index($this->params);
           return;
        } else{
            throw new Exception("Nosso suporte já está verificando, desculpe.", 500);
        }


        throw new Exception("Página não encontrada.", 404);
    }


    public function url(){

        if(isset($_GET['url'])) {

            $path = $_GET['url'];
            $path = rtrim($path, '/');
            $path = filter_var($path, FILTER_SANITIZE_URL);

            $path = explode('/', $path); //Separa a URL através de “/” e transforma em um array com a função explodedo PHP.

            $this->controller = $this->verificaArray($path, 0); //Controller
            $this->action     = $this->verificaArray($path, 1); //Action

            if($this->verificaArray($path, 2)){
                unset($path[0]);
                unset($path[1]);
                $this->params = array_values($path);
            }
            
        }
    }

    public function getController()
    {
        return $this->controller;
    }

    public function getAction()
    {
        return $this->action;
    }

    public function getControllerName()
    {
        return $this->controllerName;
    }

    public function getParams()
    {
        return $this->params;
    }

    private function verificaArray ( $array, $key ) {
        if ( isset( $array[ $key ] ) && !empty( $array[ $key ] ) ) {
            return $array[ $key ];
        }
        return null;
    }

}