<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit8d0f9f3d62e3af289ecc187c0ec2a2c9
{
    public static $prefixLengthsPsr4 = array (
        'S' => 
        array (
            'SON\\' => 4,
        ),
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'SON\\' => 
        array (
            0 => __DIR__ . '/..' . '/SON',
        ),
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/App',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit8d0f9f3d62e3af289ecc187c0ec2a2c9::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit8d0f9f3d62e3af289ecc187c0ec2a2c9::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
