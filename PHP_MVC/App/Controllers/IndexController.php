<?php

namespace App\Controllers;


use SON\Controller\Action;
use SON\DI\Container;

class IndexController extends Action
{
    public function index()
    {
        $produto = Container::getModel("Produto");
        $this->views->produtos = $produto->fetchAll();

        $this->render('index');
    }

    public function contact()
    {
        $this->view->cars = array("Mustang","Ferrari");
        $this->render('contact');
    }
}

