<?php

namespace App\Models;

class Produto
{
    protected $db;
    
    public function __construct(\PDO $db){

        $this->db = $db;
    }

    public function fetchAll(){
    
        $query = "SELECT * FROM produtos";
        return $this->db->query($query);
    }
}