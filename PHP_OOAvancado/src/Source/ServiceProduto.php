<?php 

namespace Source;

class ServiceProduto implements IServiceProduto
{

    private $db;
    private $produto;

    public function __construct(IConn $db, IProduto $produto){

        $this->db = $db->connect();
        $this->produto = $produto;
    }

    public function list(){

        $query = "select * from produtos";

        $stmt = $this->db->prepare($query);
        
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);   
    }

    public function save(){


    }

    public function update(){


    }

    public function delete(){


    }
}


 ?>