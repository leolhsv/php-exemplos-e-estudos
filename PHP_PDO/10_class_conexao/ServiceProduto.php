<?php 

class ServiceProduto
{
    private $db;
    private $produto;

    public function __construct(IConn $db, IProduto $produto){

        $this->db      = $db->connect();
        $this->produto = $produto;

    }

    public function list(){

        $query = "select * from produtos";
        $stmt = $this->db->prepare($query);
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function save(){

        $query = "INSERT INTO produtos (nome, descricao) VALUES (:nome,:descricao)";
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(":nome",$this->produto->getNome());
        $stmt->bindValue(":descricao",$this->produto->getDescricao());
        $stmt->execute();

        return $this->db->lastInsertId();
    }

    public function update(){

        $query = "UPDATE produtos set nome = ?, descricao = ? where id = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(1,$this->produto->getNome());
        $stmt->bindValue(2,$this->produto->getDescricao());
        $stmt->bindValue(3,$this->produto->getId());
        $ret = $stmt->execute();

        return $ret;
    }

    public function delete(int $id){

        $query = "DELETE FROM produtos where id = :id";
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(":id",$id);
        $ret = $stmt->execute();

        return $ret;
    }

    public function find($id){

        $query = "select * from produtos where id = :id";
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(":id",$id);
        $stmt->execute();

        return $stmt->fetch(\PDO::FETCH_ASSOC);        
    }


}

 ?>